<?php

namespace App\Http\Livewire;
use \App\Models\User;
use Livewire\WithPagination;

use Livewire\Component;

class Searchuser extends Component
{
    public $search;
    use WithPagination;

    public function render()
    {
        $users = [];
        if($this->search) {
            $users = User::where('name', 'like', "%".$this->search."%")->paginate(7);
        }    
        return view('livewire.searchuser', ['usrColl' =>$users, 'usrCount' => count($users)]);
    }
}
