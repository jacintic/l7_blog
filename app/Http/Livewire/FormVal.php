<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FormVal extends Component
{
    public $name;
    public $surnames;
    public $isValidated = false;

    protected $rules = [
        'name' => 'required|between:3,5',
        'surname' => 'required|between:2,5',
    ];

    protected $customMessages = [
        'required' => 'The :attribute field is required.',
        'between' => 'The :attribute field must contain between :min - :max characters.',
    ];

    public function submit()
    {
        $this->validate($this->rules,$this->customMessages);
        $this->isValidated = true;
    }

    public function render()
    {
        return view('livewire.form-val',['name' => $this->name, 'surnames' => $this->surnames, 'isValidated' => $this->isValidated]);
    }
}
