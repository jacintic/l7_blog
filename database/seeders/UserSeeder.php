<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Admin",
            'surnames' => '@ Strategying',
            'email' => 'admin@strategying.com', 
            'password'=> bcrypt('.x#$3cV23P4$$w02d#x.'),
            'isAdmin' => true
        ]);
    }
}