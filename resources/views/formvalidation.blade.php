<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forma Validation</title>
    @livewireStyles
    <link rel="stylesheet" href="css/bootstrap.css">
</head>

<body>
    <div class="container mt-4">
        <h3>Welcome to the form validation view</h3>
        @livewire('form-val')
    </div>
    @livewireScripts
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
</body>

</html>