<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hello World!</title>
    @livewireStyles
</head>

<body>
    <h3>Welcome to livewire display</h3>
    @livewire('counter')
    @livewire('searchuser')
    @livewireScripts
</body>

</html>