<div>
    <input wire:model="search" type="text">
    <div wire:loading.delay>
        Loading data...
    </div>
     @foreach ($usrColl as $user)
        <p>{{$user->name}}</p>
    @endforeach
    @if ($usrCount > 1)
        {{ $usrColl->links() }}
    @endif
    
</div>