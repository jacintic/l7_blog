<div>
    <form action="/userdata" method="POST"
        wire:submit.prevent="submit">
    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" class="form-control 
        @error('name')is-invalid @enderror"
            wire:model="name">
        @error('name') <span class="error">{{ $message }}</span> @enderror
    </div>
    <div>
        <label for="surnames">Surnames</label>
        <input type="text" name="surnames" id="surnames" class="form-control" 
            {{-- @unless ( @error('surnames')1 @enderror ) is-valid @endunless " --}}
            wire:model="surnames">
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" name="email" id="email" class="form-control"
            wire:model="email">
    </div>
    <div class="form-group">
        <label for="birthdate">Birth date:</label>
        <input type="datetime" name="birthdate" id="birthdate" class="form-control"
            wire:model="birthdate">
    </div>
    <div class="row">
        <div class="col">
            <label for="doctype">ID type:</label>
            <select name="idtype" id="doctype" class="form-control"
                wire:model="idtype">
                <option selected value="none">Choose...</option>
                <option value="DNI">DNI</option>
                <option value="NIE">NIE</option>
            </select>
        </div>
        <div class="col">
            <label for="doctype">ID code:</label>
             <input type="text" name="idcode" id="idcode" class="form-control"
                wire:model="idcode">
        </div>
    </div>
    <div class="mt-3">
        <button type="submit" class="btn btn-primary">Send Data</button>
    </div>
</form>
<div>
    {{$name}}
</div>
</div>